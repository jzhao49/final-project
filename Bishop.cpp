/*
 * Bishop.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */

#include "Bishop.h"

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal move shape
 * false -- if this is an illegal move shape
 */

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first - start.first;
  char dy = end.second - start.second;
  if((dx && dy) && (abs(dx) == abs(dy))){
    // if non-static and moves along a diagonal
    return true;
  }
  return false;
}
