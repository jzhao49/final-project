/*
 * Queen.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */

#include "Queen.h"

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal move shape
 * false -- if this is an illegal move shape
 */

bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first - start.first;
  char dy = end.second - start.second;
  if(dx || dy) {
    // if not both are 0
    if(abs(dx) == abs(dy)){
      // moving along diagonal
      return true;
    } else if (!dx || !dy){
      // if not moving diagonal, either dx or dy  must be 0
      return true;
    }
  }
  return false;
}
