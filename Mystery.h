///////////////////////////////////
// IT IS OK TO MODIFY THIS FILE, //
// YOU WON'T HAND IT IN!!!!!     //
///////////////////////////////////
#ifndef MYSTERY_H
#define MYSTERY_H

#include "Piece.h"

class Mystery : public Piece {

public:
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const{
    char dx = end.first-start.first;
    char dy = end.second-start.second;
    if (dx || dy){
    // if the piece moves 2 in one direction and 1 in the other
      if(abs(dx) == 2 && abs(dy) == 3){
	return true;
      } else if (abs(dy) == 2 && abs(dx) == 3){
      return true;
      }
    }
    return false;
  }  

	char to_ascii() const {
		return is_white() ? 'M' : 'm';
	}

private:
	Mystery(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // MYSTERY_H
