/*
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 * Board.h
 */
#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <map>
#include "Piece.h"
#include "CreatePiece.h"
#include "Terminal.h"
#include <algorithm>
class Board {

  // Throughout, we will be accessing board positions using an std::pair<char, char>.
  // The assumption is that the first value is the column with values in
  // {'A','B','C','D','E','F','G','H'} (all caps)
  // and the second is the row, with values in {'1','2','3','4','5','6','7','8'}
  
public:
  // Default constructor
  Board();

  //destructor
  ~Board();

  //copy constructor
  Board(const Board& original);
  
  //helper function to deallocate memory and clear board
  void clear_board();

  //equal operator for deep copies
  Board& operator=(const Board& b);
  
  // Returns a const pointer to the piece at a prescribed location if it exists,
  // or nullptr if there is nothing there.
  const Piece* operator() (std::pair<char, char> position) const;
  
  const Piece* get_piece(std::pair<char, char> position) const;
  
  // Attempts to add a new piece with the specified designator, at the given location.
  // Returns false if:
  // -- the designator is invalid,
  // -- the specified location is not on the board, or
  // -- if the specified location is occupied
  bool add_piece(std::pair<char, char> position, char piece_designator);
  
  // Displays the board by printing it to stdout
  void display() const;
  
  // Returns true if the board has the right number of kings on it
  bool has_valid_kings() const;
  
  //Returns pair location of king with color specified by param is_white
  std::pair<char,char> find_king(bool is_white) const;

  // Helper function to detect a valid move on the board
  bool is_valid_move(std::pair<char, char> start, std::pair<char, char> end, bool white) const;

  // Helper function that does everything in is_valid_move except check for check
  bool is_possible_move(std::pair<char, char> start, std::pair<char, char> end, bool white) const;
  // Helper function to move a piece with the prerequisite that is_valid_move is true
  void move_piece(std::pair<char, char> start, std::pair<char, char> end);

  // Returns true if team described by bool white is in check
  bool in_check(bool white) const;

  // Returns true if team described by bool white is in checkmate
  bool in_mate(bool white) const;

  // Returns true if team described by bool white is in stalemate
  bool in_stalemate(bool white) const;

  // Returns true if team described by bool white has any valid moves
  bool valid_moves(bool white) const;
  
private:
  // The sparse map storing the pieces, keyed off locations
  std::map<std::pair<char, char>, Piece*> occ;
};

// Write the board state to an output stream
std::ostream& operator<< (std::ostream& os, const Board& board);

#endif // BOARD_H
