/*
 * Knight.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */

#include "Knight.h"

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal move shape
 * false -- if this is an illegal move shape
 */

bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first-start.first;
  char dy = end.second-start.second;
  if (dx || dy){
    // if the piece moves 2 in one direction and 1 in the other
    if(abs(dx) == 2 && abs(dy) == 1){
      return true;
    } else if (abs(dy) == 2 && abs(dx) == 1){
      return true;
    }
  }
  return false;
}
