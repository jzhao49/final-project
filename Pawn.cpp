/*
 * Pawn.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */

#include "Pawn.h"

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal move shape
 * false -- if this is an illegal move shape
 */

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first-start.first;
  char dy = end.second-start.second;
  if(!dx){
    if (is_white()){
      if(dy == 1 || (dy == 2 && start.second == '2')){
	// white piece can move forward 1 or 2 if in 2nd row
	return true;
      }
    } else {
      if (dy == -1 || (dy == -2 && start.second == '7')){
	// black piece can move forward 1 or 2 if in 7th row
	return true;
      }
    }
  }
  return false;
}

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal capture shape
 * false -- if this is an illegal capture shape
 */

bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first-start.first;
  char dy = end.second-start.second;
  if(is_white()){
    if(dy == 1 && abs(dx) == 1){
      return true;
    }
  } else {
    if(dy == -1 && abs(dx) == 1){
        return true;
    }
  }
  return false;
}
