/*
 * Board.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */
#include <iostream>
#include <utility>
#include <map>
#include <cctype>
#include "Board.h"
#include "CreatePiece.h"
#include "Terminal.h"
#include <stdlib.h>

using std::tolower;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}



Board::Board(const Board& original){
  clear_board();
  //iterate through board that is to be copied and create copies of each piece
  for (char i = 'A'; i <= 'H'; i++){
    for (char j = '1'; j <= '8'; j++){
      if (original(std::make_pair(i,j))){
        add_piece(std::make_pair(i,j), original(std::make_pair(i,j))->to_ascii());
      }
    }
  }
}



//destructor for board, calls helper function to clear map
Board::~Board(){
  clear_board();
}



//helper function to deallocate memory for all pieces and clear board map
void Board::clear_board(){
  //iterate through each position on board. if piece is found, deallocate memory and erase from map occ
  for (char col = 'A'; col <= 'H'; col++){
    for (char row = '8'; row >= '1'; row--){
      if ((*this)(std::make_pair(col,row))){
	delete occ[std::make_pair(col,row)];
	occ.erase(std::make_pair(col,row));
      }
    }
  }
}



//returns pointer to piece at specified input position (pair) or null if no piece is there
const Piece* Board::operator()(std::pair<char, char> position) const {
  if (occ.find(position) != occ.end()){
    return occ.at(position);
  }
  else { return nullptr; }
}

const Piece* Board::get_piece(std::pair<char, char> position) const{
 if (occ.find(position) != occ.end()){
    return occ.at(position);
  }
  else { return nullptr; }
}



//adds piece (piece type determined by second argument (char)) to specific location determined by first argument (pair)
bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  std::string designators = "kqbnrpm";
  //if piece_designator is invalid
  if (designators.find_first_of(tolower(piece_designator)) == std::string::npos){
    return false;
  }

  //check if position is on the board
  if (position.first < 'A' || position.first > 'H'){
    return false;
  }

  if (position.second < '1' || position.second > '8'){
    return false;
  }

  //make sure position is empty
  if (((*this)(position))){
    return false;
  }

  //add piece
  occ[position] = create_piece(piece_designator);
  return true;
}



//find the location of king with color specified by parameter is_white
std::pair<char, char> Board::find_king(bool is_white) const{
  char king = (is_white ? 'K' : 'k');
  if(has_valid_kings()){
    //iterate through map to find key of desired king piece
    for (std::map<std::pair<char, char>, Piece*>::const_iterator it = occ.cbegin(); it != occ.cend(); ++it){
      if (it->second->to_ascii() == king){
	return it->first;
      }
    }
  }
  return std::make_pair(0, 0);
}

//check to see if the board has the correct number of kings and return corresponding boolean value 
bool Board::has_valid_kings() const {
  int wkings = 0;
  int bkings = 0;
  //count up black and white kings
  for (std::map<std::pair<char, char>, Piece*>::const_iterator it = occ.cbegin(); it != occ.cend(); ++it){
    if (it->second->to_ascii() == 'k'){
      bkings++;
    }
    if (it->second->to_ascii() == 'K'){
      wkings++;
    }
  }

  //make sure there's  one black and one white king
  if (wkings==1 && bkings==1){
    return true;
  }
  return false;
}



//display board to user
void Board::display() const {
  std::cout << std::endl;
  //iterate through entire board
  for (char row = '8'; row >= '1'; row--){
    //print row labels
    Terminal::color_fg(0, Terminal::YELLOW);
    std::cout << row;
    Terminal::set_default();
    for (char col = 'A'; col <= 'H'; col++){
      //display alternating pattern of colors so that each queen is on her color
      if ((row+col)%2 == 0){
	Terminal::color_all(0, Terminal::WHITE, Terminal::BLUE);
      } else { Terminal::color_all(0, Terminal::BLUE, Terminal::WHITE);}

      //print piece designator if current location holds one, if not print '-'
      if (occ.find(std::make_pair(col,row))!= occ.end()){
	std::cout << occ.at(std::make_pair(col,row))->to_ascii() << " ";
      } else { std::cout << "  "; }
    }
    Terminal::set_default();
    std::cout << std::endl;
  }
  //print column labels
  std::cout << " ";
  for (char c = 'A'; c <= 'H'; c++){
    Terminal::color_fg(0, Terminal::YELLOW);
    std::cout << c << " ";
    Terminal::set_default();
  }
  std::cout << std::endl;
  Terminal::set_default();
}



//equals operator for deep copies; parameter is board with desired contents that are to be copied
Board& Board::operator=(const Board& b){
  clear_board();
  //iterate through board that is to be copied and create copies of each piece
  for (char i = 'A'; i <= 'H'; i++){
    for (char j = '1'; j <= '8'; j++){
      if (b(std::make_pair(i,j))){
	add_piece(std::make_pair(i,j), b(std::make_pair(i,j))->to_ascii());
      }
    }
  }
  return *this;
}



/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
//writes Board board out to ostream os
std::ostream& operator<<(std::ostream& os, const Board& board) {
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = board(std::pair<char, char>(c, r));
      if (piece) {
	os << piece->to_ascii();
      } else {
	os << '-';
      }
    }
    os << std::endl;
  }
  return os;
}



/*
 * is_possible_move
 * Parameters:
 * start -- pair<char, char> giving start position of potential move
 * end -- pair<char, char> giving end position of potential move
 * white -- bool that describes whose turn it is
 * Return:
 * true -- this is a physically possible move (valid move/capture shape and does not move through pieces if linear)
 * false -- if not a physicall possible move
 */

bool Board::is_possible_move(std::pair<char, char> start, std::pair<char, char> end, bool white) const{

  Piece const* p = get_piece(start);
  Piece const* capture = get_piece(end);

  // Return false if start and end positions are not on the board

  if(start.first < 'A' || start.first > 'H' || start.second < '1' || start.second > '8'||
     end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8'){
    return false;
  }

  // Return false if start position is empty or occupied by piece of wrong color
  if(p == nullptr || p->is_white() != white) {
    return false;
  }

  // Return false if
  if(capture == nullptr) {
    // No piece exists in the end position, and it is not a legal move
    if(!p->legal_move_shape(start, end)) {
      return false;
    }
  } else {
    // A piece of the same color is in the end position
    if(capture->is_white() == white) {
      return false; 
    }
    // A piece of the opposite color is in the end position and it is not a legal capture
    if(!p->legal_capture_shape(start, end)) {
      return false;
    }
  }

  // Test for moving through pieces -- get change in x and y
  char dx = end.first - start.first;
  char dy = end.second - start.second;

  // Return false if the piece is not moved anywhere
  if(!dx && !dy) return false;

  // Hen hao !!
  // Return false if there's a piece in the way along linear start and end
  if(!dx || !dy || abs(dx) == abs(dy)) { // if moves only vert, hori, or diagonally
    // assign dirx either to -1, 0, 1 based on dx 
    char dirx;
    if(dx){
      dirx = dx/abs(dx);
    } else {
      dirx = 0;
    }
    // assign diry either to -1, 0, 1 based on dy
    char diry;
    if(dy){
      diry = dy/abs(dy);
    } else{
      diry = 0;
    }
    
    std::pair<char, char> pos;
    // iterate along the line and get new position -- return false if board contains a piece there
    for(int i = 1; i < std::max(abs(dx), abs(dy)); i++){
      pos = std::make_pair(start.first+dirx*i, start.second+diry*i);
      if(get_piece(pos)){
	return false;
      }
    } 
  }
  return true;
}



/*
 * is_valid_move
 * Parameters:
 * start -- pair<char, char> gives starting position
 * end -- pair<char, char> gives end position
 * white -- bool whether it is white's turn or not
 * Return:
 * true -- if a piece can move a new position with its king being in check
 * false otherwise
 */
bool Board::is_valid_move(std::pair<char, char> start, std::pair<char, char> end, bool white) const{
  // if the piece can physically move from start to end
  if(is_possible_move(start, end, white)){

    // create a copy of the board
    Board copy(*this);

    // move piece on the board
    copy.move_piece(start, end);

    // return false if in_check is true
    if(copy.in_check(white)){
      return false;
    }

    // otherwise, return true
    return true;
  }
  return false;
}



/*
 * move_piece
 * Paramters:
 * start -- pair<char, char> gives starting position
 * end -- pair<char, char> gives end position
 * white -- bool whether it is white's turn or not
 * Return: void
 * Function: Moves a piece on the board from the start to position to end position
 * Prerequisite: start->end must be a possible move
 */
void Board::move_piece(std::pair<char, char> start, std::pair<char, char> end) {
  // get piece at the end position and deallocate it
  Piece* p = occ[end];
  delete p;
  p = nullptr;
  // set end position to point to piece at start position
  occ[end] = occ.at(start);

  // remove start key from occ
  occ.erase(start);

  // promotion check
  if(end.second == '8' && occ[end]->to_ascii() == 'P'){
    // if a white pawn is in the eigth row, delete and replace with whtie queen
    p = occ[end];
    delete p;
    p = nullptr;
    occ.erase(end);
    occ[end] = create_piece('Q');
  } else if (end.second == '1' && occ[end]->to_ascii() == 'p'){
    // if a black pawn is in the 1st row, delete and replace with a black queen
    p = occ[end];
    delete p;
    occ.erase(end);
    p = nullptr;
    occ[end] = create_piece('q');
  }
}



/*
 * in_check
 * Parameters:
 * bool white-- which team to check for check
 * Return:
 * true -- if in check
 * false -- otherwise
 */
bool Board::in_check(bool white) const{
  // get king's position
  std::pair<char, char> king_pos = find_king(white);

  // iterate over pieces and see king's position is a possible move for any piece
  for(std::map<std::pair<char, char>, Piece*>::const_iterator cit = occ.cbegin();
      cit != occ.cend();
      cit++){
    if(cit->second->is_white() != white){
      if(is_possible_move(cit->first, king_pos, !white)){
	return true;
      }
    }
  }
  return false;
}



/*
 * valid_moves
 * Parameters:
 * bool white-- which team to check for valid_moves
 * Return:
 * true -- if team has any valid moves
 * false -- otherwise
 */
bool Board::valid_moves(bool white) const{
  std::pair<char, char> end;

  // iterate through pieces
  for(std::map<std::pair<char, char>, Piece*>::const_iterator cit = occ.cbegin();
      cit != occ.cend();
      cit++) {
    if(cit->second->is_white() == white){ // if piece is of desired team
      for(char i = 'A'; i < 'I'; i++){
	for(char j = '1'; j < '9'; j++){
	  // iterate through entire board
	    end = std::make_pair(i, j);

	    // return true if the piece can make a valid move to a position on the board
	    if(is_valid_move(cit->first, end, white)){
	      return true;
	    }
	} 
      }
    }
  }
  return false;
}



/*
 * in_mate
 * Parameters:
 * bool white-- which team to check for mate
 * Return:
 * true -- if in checkmate
 * false -- otherwise
 */
bool Board::in_mate(bool white) const{
  if(in_check(white)){ // if team is in check and cannot make any valid moves, return true
    return !valid_moves(white);
  }
  return false;
}



/*
 * in_stalemate
 * Parameters:
 * bool white-- which team to check for stalemate
 * Return:
 * true -- if in stalemate
 * false -- otherwise
 */
bool Board::in_stalemate(bool white) const{
  if(!in_check(white)){ // if team is not in check but cannot make any valid moves, return true
    return !valid_moves(white);
  }
  return false;
}
