/*
 * Chess.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */
#include "Chess.h"

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

/*
 * make_move
 * Parameters:
 * start -- pair<char, char> start position of potential move
 * end -- pair<char, char> end position of potential move
 * Return:
 * true -- if move was made
 * false otherwise
 */
bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
  if(board.is_valid_move(start, end, is_white_turn)) {
    board.move_piece(start, end);
    is_white_turn = !is_white_turn;
    return true;
  }
  return false;
}


// Returns true if bool white is in check
bool Chess::in_check(bool white) const {
  return board.in_check(white);
}

// Returns true if bool white is in checkmate
bool Chess::in_mate(bool white) const {
  return board.in_mate(white);
}

// Returns true if bool game is in stalemate
bool Chess::in_stalemate(bool white) const {
  return board.in_stalemate(white);
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}

// UNUSED -- helper function to overwrite current board with contents of different board (first parameter). Second parameter is color of next turn
void Chess::overwrite_board(Board& b, char color){
  //copy contents of diff board into curr board
  board = b;
  //determine color of next turn
  if (color == 'w'){
    is_white_turn = true;
  } else {
    is_white_turn = false;
  }
}


//input stream operator to load chess games
std::istream& operator>> (std::istream& is, Chess& chess) {

  Board b;   //new board to load contents into
  std::string input;
  int row = 0;
  char color;
  //store each row as a string and iteratively load contents of each into board
  while(is >> input){

    //handle char that designates who's turn it is
    if (input.length() == 1){
      color = input[0];
      break;
    }

    //create pieces from file text to load into board
    for (int col = 0; col < 8; col++){
      char letter = 'A'+col;
      char num = '8'-row;
      b.add_piece(std::make_pair(letter,num),input[col]);
    }
    row++;
    input.empty();
  }
  //overwrite current board with new board contents
  chess.overwrite_board(b, color);

  return is;
}  
