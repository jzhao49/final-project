/*
 * King.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */

#include "King.h"

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal move shape
 * false -- if this is an illegal move shape
 */
bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first-start.first;
  char dy = end.second-start.second;
  if((dx || dy) && (abs(dx) == 1 || dx == 0) && (abs(dy) == 1 || dy == 0)){
    // if it moved in the x or y direction (not static)
    // if it moved 1 or 0 in x direction
    // if it moved 1 or 0 in the y direction
    return true;
  }
  return false;
}
