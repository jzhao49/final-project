/*
 * Rook.cpp
 * Alex Wei, Justin Zhao, Oliver Valera
 * awei11, jzhao49, ovalera1
 */

#include "Rook.h"

/*
 * legal_move_shape
 * Parameters:
 * start -- pair<char, char> giving the star position
 * end -- pair<char, char> gives the end position
 * Return:
 * true -- if this is a legal move shape
 * false -- if this is an illegal move shape
 */

bool Rook::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  char dx = end.first-start.first;
  char dy = end.second-start.second;
  if(dx || dy){
    // if not a static movement
    if(!dx || !dy){
      // if one of them is 0-- can only move along vert and hori
      return true;
    }
  }
  return false;
}
