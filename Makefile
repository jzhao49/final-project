CC=g++
CFLAGS=-Wall -Wextra -std=c++11 -pedantic -g

chess:  Board.o Chess.o main.o CreatePiece.o Rook.o King.o Queen.o Pawn.o Bishop.o Knight.o
	$(CC) -o chess Board.o Chess.o main.o CreatePiece.o Rook.o King.o Queen.o Pawn.o Bishop.o Knight.o -g

PieceTest: PieceTest.o Pawn.o Rook.o Bishop.o Knight.o Queen.o King.o CreatePiece.o
	$(CC) -o PieceTest PieceTest.o Pawn.o Rook.o Bishop.o Knight.o Queen.o King.o CreatePiece.o -g

Board.o: Board.cpp Board.h CreatePiece.h Terminal.h
	$(CC) $(CFLAGS) -c Board.cpp -g

main.o: main.cpp Chess.h
	$(CC) $(CFLAGS) -c main.cpp

Chess.o: Chess.cpp Chess.h
	$(CC) $(CFLAGS) -c Chess.cpp

CreatePiece.o: CreatePiece.h CreatePiece.cpp Pawn.h Rook.h King.h Queen.h Knight.h Bishop.h Mystery.h
	$(CC) $(CFLAGS) -c CreatePiece.cpp

Rook.o: Rook.h Rook.cpp
	$(CC) $(CFLAGS) -c Rook.cpp

Pawn.o: Pawn.h Pawn.cpp
	$(CC) $(CFLAGS) -c Pawn.cpp

Knight.o: Knight.h Knight.cpp
	$(CC) $(CFLAGS) -c Knight.cpp

Bishop.o: Bishop.h Bishop.cpp
	$(CC) $(CFLAGS) -c Bishop.cpp

Queen.o: Queen.h Queen.cpp
	$(CC) $(CFLAGS) -c Queen.cpp

King.o: King.h King.cpp
	$(CC) $(CFLAGS) -c King.cpp

PieceTest.o: PieceTest.cpp Pawn.h Rook.h Bishop.h Knight.h Queen.h King.h Mystery.h CreatePiece.h
	$(CC) -c PieceTest.cpp $(CFLAGS)

clean:
	rm -f *.o chess PieceTest
